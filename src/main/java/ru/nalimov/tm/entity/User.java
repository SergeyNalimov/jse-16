package ru.nalimov.tm.entity;

import ru.nalimov.tm.enumerated.Role;
import ru.nalimov.tm.util.HashMD5;

import java.util.Objects;

public class User {

    private Long id = System.nanoTime();

    private String login = "";

    private String password = "";

    private String firstName = "";

    private String secondName = "";

    private String lastName = "";

    private Role userRole = Role.USER;

    private boolean admin_true = false;

    public User(){}

    public User(final String login, final String userPassword, final String firstName, final String secondName, final String lastName, final Role userRole) {
        this.login = login;
        this.password = HashMD5.getHash(userPassword);
        this.firstName = firstName;
        this.secondName = secondName;
        this.lastName = lastName;
        this.userRole = userRole;
    }

    public User(final String login, final String userPassword, final String firstName, final String secondName, final String lastName) {
        this.login = login;
        this.password = HashMD5.getHash(userPassword);
        this.firstName = firstName;
        this.secondName = secondName;
        this.lastName = lastName;
        this.userRole = Role.USER;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getHashPassword() {
        return password;
    }

    public void setHashPassword(String password){
        this.password = HashMD5.getHash(password);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Role getUserRole() {
        return userRole;
    }

    public void setUserRole(Role userRole) {
        this.userRole = userRole;
    }


    public boolean isAdmin_true() { return admin_true; }

    public void setAdmin_true(boolean admin_true) { this.admin_true = admin_true; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(login, user.login) &&
                Objects.equals(password, user.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, password);
    }

}
